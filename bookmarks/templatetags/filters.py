from django import template

register = template.Library()


@register.filter(name='screen_self')
def screen_self(passed_user, current_user):
    if current_user.username == passed_user.username:
        return ''
    else:
        return ''

@register.filter
def screen_same(thing, other):
	if thing == other:
		return ''
	else:
		return thing
