from django import template

register = template.Library()


@register.filter(name='shorten_title')
def shorten_title(title):
    length = len(title)
    if length > 30:
        return title[0:30] + "..."
    else:
        return title