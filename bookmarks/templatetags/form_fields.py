from django import template

register = template.Library()


@register.inclusion_tag('form_snippet.html')
def my_field(field_var):
    return {'field': field_var}

@register.inclusion_tag('day_form_snippet.html')
def day_field(day_dict, day_int, sche_form):
    day_name = day_dict[day_int]
    openfield_starter = "open_time_{}".format(str(day_int))
    closefield_starter = "close_time_{}".format(str(day_int))
    openfield  = sche_form[openfield_starter]
    closefield = sche_form[closefield_starter]
    return {'day_name': day_name, 'openfield': openfield, 'closefield': closefield}