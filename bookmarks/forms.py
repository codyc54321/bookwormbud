import requests, re
from lxml import html

from django                         import forms
from django.forms.widgets           import Select
from django.core.exceptions         import ValidationError
from django.contrib.auth.models     import User
from django                         import forms
from django.template.defaultfilters import slugify
from django.contrib.admin.widgets   import AdminDateWidget
from django.forms.extras.widgets    import SelectDateWidget
from django.forms.fields            import DateField

from bookmarks.models       import Article, MyTag
from bookmarks.functions    import ensure_http

def process_title(url):
    def _search_for_title(url):
        url = ensure_http(url)
        try:
            r = requests.get(url)
            content = r.text
            t = html.document_fromstring(content)
            return t.find(".//title").text
        except:
            return None

    title = _search_for_title(url)
    print(title)
    return title or "Couldn't find a title"

#----------------------------------------------------------------------------------------------------

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ('url', 'title', 'user')

    url     = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'required'}),
                  max_length=256, label='URL', required=True)
    title   = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "I'll grab the page's title"}),
                  max_length=256, label='Title', required=False)
    user    = forms.CharField(widget=forms.HiddenInput())

    def clean(self):
        title = self.cleaned_data['title']
        url   = self.cleaned_data['url']
        title = process_title(url)
        self.cleaned_data['title'] = title
        self.instance.title = title
        username = self.cleaned_data['user']
        user = User.objects.get(username=username)
        self.cleaned_data['user'] = user
        super(ArticleForm, self).clean()
        return self.cleaned_data

#----------------------------------------------------------------------------------------------------

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ('url', 'title', 'user')

    url     = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'required'}),
                  max_length=256, label='URL', required=True)
    title   = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "I'll grab the page's title"}),
                  max_length=256, label='Title', required=False)
    user    = forms.CharField(widget=forms.HiddenInput())

    def clean(self):
        try:
            title = self.cleaned_data['title']
            url   = self.cleaned_data['url']
            title = process_title(url)
            self.cleaned_data['title'] = title
        except:
            pass
        username = self.cleaned_data['user']
        user = User.objects.get(username=username)
        self.cleaned_data['user'] = user
        super(ArticleForm, self).clean()
        return self.cleaned_data

#----------------------------------------------------------------------------------------------------

class EditArticleForm(forms.ModelForm):
    url     = forms.CharField(required=False)
    title   = forms.CharField(required=False)
    class Meta:
        model = Article
        fields = ('url', 'title')

#----------------------------------------------------------------------------------------------------

class TagForm(forms.ModelForm):
    name = forms.CharField(label="", required=False)
    class Meta:
        model = MyTag
        fields = ('name',)

    def clean_name(self):
        name = self.cleaned_data['name']
        if not name:
            raise ValidationError("Name is empty")
        return slugify(name)


def get_tag_choices():
    #reading tags to get all values
    tags = MyTag.objects.all()#collect tags
    CATEGORY_CHOICES = [('', '')]#default selection
    if tags:
        for tag in tags:
            CATEGORY_CHOICES.append((tag.name, tag.name))
    return CATEGORY_CHOICES

#----------------------------------------------------------------------------------------------------
#this class uses preformatted fields, like model,
#just setting up structure of a multiple choice field
class TagSelectForm(forms.ModelForm):
    class Meta:
        model = MyTag
        fields = ('name',)

    name = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=get_tag_choices()
    )



class AddedDateCalendarForm(forms.Form):
    added_start_date  = forms.DateField()
    added_end_date    = forms.DateField()


class LastViewedDateCalendarForm(forms.Form):
    last_viewed_start_date  = forms.DateField()
    last_viewed_end_date    = forms.DateField()
    # field1 = forms.DateField(widget=SelectDateWidget(empty_label="Nothing"))

    # field2 = forms.DateField(
    #     widget=SelectDateWidget(
    #         empty_label=("Choose Year", "Choose Month", "Choose Day"),
    #     ),
    # )

    # A custom empty label with tuple
    # field2 = forms.DateField()
